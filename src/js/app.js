import $ from 'jquery';
import 'bootstrap';
import './module/clicker';
import{API_KEY}from './constants';
import'./src/html/map.html';
import'./src/html/window.html';

let script= document.createElement('script');
script.src = 'https://maps.googleapis.com/maps/api/js?key=${API_KEY}&callback=initMap';
script.defer = true;
script.async = true;

const menuTrigger =$('.hamburger-menu');
const closeTrigger = $('.close-btn');
const body = $('body');
let menuOpen = false;

menuTrigger.on('click',function(){
  if(menuOpen === false){
body.addClass('open-menu');
menuOpen = true;
  }
})


closeTrigger.on('click',function(){
  if(menuOpen === true) {
    body.removeClass('open-menu');
    menuOpen = false;
  }
})


window.initMap=function() {
  let qingdao={lat:36.0336, lng:120.2002};
  let map;

  map=new google.maps.Map(document.getElementById('map'),{
    center:qingdao
    zoom:12
});

    var contentString = '
    <div id="content">'+

      <h1 id="firstHeading" class="firstHeading">qingdao</h1>
      <div id="bodyContent">
      <p>
      Famous traveling city around the world
      </p>
      </div>
      </div>;

      var infowindow = new google.maps.InfoWindow({
    content: contentString
  })


  var marker=new google.map.Harker({position:qingdao,map:map,
     animation: google.maps.Animation.DROP,
    title:"Qingdao"
  });

  marker.addListener('click', function() {
   infowindow.open(map, marker);
 });
}

document.head.appendChild(script);




window.$ = $;
window.jQuery = $;
